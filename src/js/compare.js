var Highcharts = require('highcharts');
var modalInit = require('./components/_modal');
var moodBlock = require('./components/_mood');

$(function() {
    // Init modal
    modalInit();
    moodBlock.init();
    // Generate the chart 
	Highcharts.chart('js-chart', {
        chart: {
		    backgroundColor: '#000',
		    
		    spacingBottom: 0,
		    spacingLeft: 0,
		    spacingRight: 0
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['24HRS', '48HRS', '72HRS', '96HRS'],
            lineColor: '#2f2f2f',
            tickLength: 0,
            labels: {
            	style: {
            		'color': '#fff',
            		'fontFamily': 'Oswaldo, sans-serif',
            		'fontSize': '13px',
            	}
            }
        },
        yAxis: {
            title: {
                text: ''
            },
            labels: {
            	enabled: false
            },
            gridLineColor: '#2f2f2f'
        },
        legend: {
            enabled: false
        },
        series: [{
                    name: 'Manager1',
                    data: [4, 11, 4, 11],
                    color: '#f26322',
                    marker: {
                        enabled: true,
                        radius: 5,
                        symbol: 'square',
                        fillColor: '#ec6324'
                    },
                },
                {
                    name: 'Manager2',
                    data: [11, 4, 11, 4],
                    color: '#1d89cd',
                    marker: {
                        enabled: true,
                        radius: 5,
                        symbol: 'square',
                        fillColor: '#1d89cd'
                    },
                }
        ],
        navigation: {
        	buttonOptions: {
        		enabled: false
        	}
        },
        credits: {
        	enabled: false
        }
    });

    // Mood block animation

});