var moodBlock = function() {
	// Options
	var activeClass = "controls--item__active";
	var animDuration = 500;
	// Blocks and controls (first array element - active)
	var blocks = [],
		controls = [],
		positiveValue = [],
		negativeValue = [],
		isAnimating = false;
	var init = function() {
		// Get blocks and controls
		$('.js-mood-block').each(function(index, item) {
			blocks.push(item);
		});
		$('.js-mood-control').each(function(index, item) {
			controls.push(item);
		});
		// Get values
		for (var i = 0; i < blocks.length; i++) {
			positiveValue.push(
				$(blocks[i]).data('positive')
			);
			negativeValue.push(
				$(blocks[i]).data('negative')
			);
		}
		// Initial animation
		$(blocks[0]).find('.js-positive-bar').animate({'height': positiveValue[0]}, animDuration);
		$(blocks[0]).find('.js-negative-bar').animate({'height': negativeValue[0]}, animDuration);
		// Attach event
		$('.js-mood-control').click(toggle);
	}
	var toggle = function(e) {
		var control = $(e.target);
		if(control.hasClass(activeClass) || isAnimating) {
			return false;
		}
		// Change block state
		isAnimating = true;
		// Toggle controls class
		$('.' + activeClass).removeClass(activeClass);
		$(controls[1]).addClass(activeClass);
		// Animation
		$(blocks[0]).find('.js-positive-bar').animate({'height': 0}, animDuration/2, function() {
			$(blocks[1]).find('.js-positive-bar').animate({'height': positiveValue[1]}, animDuration);
		});
		$(blocks[0]).find('.js-negative-bar').animate({'height': 0}, animDuration/2, function() {
			$(blocks[0]).css('display', 'none');
			$(blocks[1]).css('display', 'flex');
			$(blocks[1]).find('.js-negative-bar').animate({'height': negativeValue[1]}, animDuration, function() {
				// Reverse arrays
				blocks.reverse();
				controls.reverse();
				positiveValue.reverse();
				negativeValue.reverse();
				isAnimating = false;
			});
		});
	}
	return {
		init: init
	}
}

module.exports = moodBlock();