require('tinyscrollbar');

var modalInit = function() {
	// Scrollbar init
	var modalScrollbar = $('#js-scrollbar');
    modalScrollbar.tinyscrollbar();
    var scrollbarInstance = modalScrollbar.data("plugin_tinyscrollbar");
    $(window).resize(function() {
        scrollbarInstance.update();
    });
    // Modal window init
	$('.js-modal-open').click(function() {
        $('body').addClass('modal-opened');
        $('.js-modal').fadeIn();
        scrollbarInstance.update();
        var selected = 0;
        $('.js-radio').click(function(e) {
            e.preventDefault();

        });
        $('.js-item').click(function() {
            var radio = $(this).find('.js-radio');
            if(radio.prop("checked")) {
                selected--;
                radio.prop("checked", false);
            }
            else {
                if(selected < 2) {
                    selected++;
                    radio.prop("checked", true);
                }
            }
        });
        $('.js-modal-close').click(function() {
            $('.js-modal').fadeOut();
            $('body').removeClass('modal-opened');
        });
    });
}

module.exports = modalInit;