var Highcharts = require('highcharts');
var modalInit = require('./components/_modal');
var moodBlock = require('./components/_mood');

$(function() {
    // Init modal
    modalInit();
    // Init mood block
    moodBlock.init();
	// Generate the chart 
	Highcharts.chart('js-chart', {
        chart: {
		    backgroundColor: '#000',
		    
		    spacingBottom: 0,
		    spacingLeft: 0,
		    spacingRight: 0
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['24HRS', '48HRS', '96HRS'],
            lineColor: '#2f2f2f',
            tickLength: 0,
            labels: {
            	style: {
            		'color': '#fff',
            		'fontFamily': 'Oswaldo, sans-serif',
            		'fontSize': '13px',
            	}
            }
        },
        yAxis: {
            title: {
                text: ''
            },
            labels: {
            	enabled: false
            },
            gridLineColor: '#2f2f2f'
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Manager',
            data: [7.0, 6.9, 9.5],
            color: '#f26322',
            marker: {
                enabled: true,
                radius: 5,
                symbol: 'square',
                fillColor: '#ec6324'
            },
        }],
        navigation: {
        	buttonOptions: {
        		enabled: false
        	}
        },
        credits: {
        	enabled: false
        }
    });
});